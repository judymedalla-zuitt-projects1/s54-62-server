/*
===========================================
S48 - Express.js - API Development (Part 5)
===========================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	What is ChatGPT?

	Improving your prompts

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Integrate and contextualize the generated route code from the chatGPT conversation.
	Application > routes > user.js
*/
		/*
			1a. Copy and paste the generated route code from the chatGPT conversation.
				Application > routes > user.js
		*/

			/*...*/

			// [SECTION] POST route for resetting the password
			router.post('/reset-password', authMiddleware, resetPasswordController.resetPassword);


			module.exports = router;


		/*
			1b. Update and contextualize our code.
				Application > routes > user.js
		*/
			//Update authMiddleware to our own auth module and use verify instead.
			//Update resetPasswordController to userController, our own controller module instead.
			//Change the route to put as this is an edit of a document.
			router.put('/reset-password', verify, userController.resetPassword);

/*
2. Integrate and contextualize the generated controller code from the chatGPT conversation.
	Application > controllers > user.js
*/
		/*
			2a. Copy and paste the generated controller code from the chatGPT conversation.
				Application > controllers > user.js
		*/

			/*...*/

			const resetPassword = async (req, res) => {
				try {
				const { newPassword } = req.body;
				const { userId } = req.user; // Extracting user ID from the authorization header
			
				// Hashing the new password
				const hashedPassword = await bcrypt.hash(newPassword, 10);
			
				// Updating the user's password in the database
				await User.findByIdAndUpdate(userId, { password: hashedPassword });
			
				// Sending a success response
				res.status(200).json({ message: 'Password reset successfully' });
				} catch (error) {
				console.error(error);
				res.status(500).json({ message: 'Internal server error' });
				}
			};
			
			module.exports = {
				resetPassword
			};

		/*
			2b. Contextualize generated controller code from the chatGPT conversation.
				Application > controllers > user.js
		*/
		
		//Modify how we export our controllers
		module.exports.resetPassword = async (req, res) => {
			try {

			//Add a console.log() to check if you can pass data properly from postman
			//console.log(req.body);

			//Add a console.log() to show req.user, our decoded token, does not contain userId property but instead id
			//console.log(req.user);

			const { newPassword } = req.body;

			//update userId to id because our version of req.user does not have userId property but id property instead.
			const { id } = req.user; // Extracting user ID from the authorization header
		
			// Hashing the new password
			const hashedPassword = await bcrypt.hash(newPassword, 10);
			
			//Update userId update to id
			// Updating the user's password in the database
			await User.findByIdAndUpdate(id, { password: hashedPassword });
		
			// Sending a success response
			res.status(200).json({ message: 'Password reset successfully' });
			} catch (error) {
			console.error(error);
			res.status(500).json({ message: 'Internal server error' });
			}
		};

		/*
			Important Note:
				- Use postman to test the routes.
				- Make sure students uses their own databases.
				- They can clone your pushed discussion but make sure to npm install first.
		*/


/*
3. Login and copy a token to process a POST request at the '/reset-password' route using postman update a user's password.
	Postman
*/

		url: http://localhost:4000/users/reset-password
		method: PUT
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
		body: raw + JSON
			{
				"newPassword" : "newPass1234"
			}

		/*
			Important Note:
				- Use postman to test the route then try to login with the old password
		*/

/*

/*
4. Integrate and contextualize the generated route code from the next chatGPT conversation.
	Application > routes > user.js
*/
		/*
			4a. Copy and paste the generated route code from the next chatGPT conversation.
				Application > routes > user.js
		*/

			/*...*/

			//[SECTION] Update user profile route
			router.put('/profile', authMiddleware.authenticateToken, profileController.updateProfile);


			module.exports = router;


		/*
			4b. Update and contextualize our code.
				Application > routes > user.js
		*/
			//Update authMiddleware.authenticateToken to our own auth module and use verify instead.
			//Update profileController to userController, our own controller module instead.
			router.put('/profile', verify, userController.updateProfile);

/*

/*
6. Integrate and contextualize the generated controller code from the chatGPT conversation.
	Application > controllers > user.js
*/
		/*
			6a. Copy and paste the generated controller code from the chatGPT conversation.
				Application > controllers > user.js
		*/

		/*...*/

		async function updateProfile(req, res) {
			try {
			  // Get the user ID from the authenticated token
			  const userId = req.user.id;
		  
			  // Retrieve the updated profile information from the request body
			  const { firstName, lastName, mobileNumber } = req.body;
		  
			  // Update the user's profile in the database
			  const updatedUser = await User.findByIdAndUpdate(
				userId,
				{ firstName, lastName, mobileNumber },
				{ new: true }
			  );
		  
			  res.json(updatedUser);
			} catch (error) {
			  console.error(error);
			  res.status(500).json({ message: 'Failed to update profile' });
			}
		  }
		  
		  module.exports = {
			updateProfile,
		  };

	/*
		6b. Contextualize generated controller code from the chatGPT conversation.
			Application > controllers > user.js
	*/
		//Update the function to arrow to unify our code formats
		//Modify how we export our controllers
		module.exports.updateProfile = async (req, res) => {
			try {

			//Add a console.log() to check if you can pass data properly from postman
			//console.log(req.body);

			//Add a console.log() to show req.user, our decoded token, does have id property
			//console.log(req.user);
				
			// Get the user ID from the authenticated token
			const userId = req.user.id;
		
			// Retrieve the updated profile information from the request body
			// Update the req.body to use mobileNo instead of mobileNumber to match our schema
			const { firstName, lastName, mobileNo } = req.body;
		
			// Update the user's profile in the database
			const updatedUser = await User.findByIdAndUpdate(
				userId,
				{ firstName, lastName, mobileNo },
				{ new: true }
			);
		
			res.send(updatedUser);
			} catch (error) {
			console.error(error);
			res.status(500).send({ message: 'Failed to update profile' });
			}
		}

	/*
		Important Note:
			- Use postman to test the routes.
			- Make sure students uses their own databases.
			- They can clone your pushed discussion but make sure to npm install first.
	*/


/*

/*
7. Login and copy a token to process a POST request at the '/profile' route using postman update a user's details.
	Postman
*/

	url: http://localhost:4000/users/profile
	method: PUT
	headers:
		Type: Bearer Token
		Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
	body: raw + JSON
		{
			"firstName": "updatedFName",
			"lastName": "updatedLName",
			"mobileNum": "09261234123"
		}

	/*
		Important Note:
			- Use postman to test the route then try to check mongodb
	*/
/*
8. Integrate and contextualize the generated route code from the next chatGPT conversation.
	Application > routes > course.js
*/
		/*
			8a. Copy and paste the generated route code from the next chatGPT conversation.
				Application > routes > course.js
		*/

			/*...*/

			//[SECTION] Update user profile route
			router.post('/courses/search', courseController.searchCoursesByName);


			module.exports = router;


		/*
			8b. Update and contextualize our code.
				Application > routes > user.js
		*/
			//Update endpoint to remove /courses as we already group our routes under this in index.js
			router.post('/search', courseController.searchCoursesByName);

/*

/*
9. Integrate and contextualize the generated controller code from the chatGPT conversation.
	Application > controllers > course.js
*/
		/*
			9a. Copy and paste the generated controller code from the chatGPT conversation.
				Application > controllers > course.js
		*/

		/*...*/

		const searchCoursesByName = async (req, res) => {
			try {
			  const { courseName } = req.body;
		  
			  // Use a regular expression to perform a case-insensitive search
			  const courses = await Course.find({
				name: { $regex: courseName, $options: 'i' }
			  });
		  
			  res.json(courses);
			} catch (error) {
			  console.error(error);
			  res.status(500).json({ error: 'Internal Server Error' });
			}
		  };
		  
		  module.exports = {
			searchCoursesByName
		  };

	/*
		9b. Contextualize generated controller code from the chatGPT conversation.
			Application > controllers > course.js
	*/
		//Update the function to arrow to unify our code formats
		//Modify how we export our controllers
		module.exports.searchCoursesByName = async (req, res) => {
			try {
			  const { courseName } = req.body;
		  
			  // Use a regular expression to perform a case-insensitive search
			  const courses = await Course.find({
				name: { $regex: courseName, $options: 'i' }
			  });
		  
			  res.json(courses);
			} catch (error) {
			  console.error(error);
			  res.status(500).json({ error: 'Internal Server Error' });
			}
		};

	/*
		Important Note:
			- Use postman to test the routes.
			- Make sure students uses their own databases.
			- They can clone your pushed discussion but make sure to npm install first.
	*/


/*
/*
10. Login and copy a token to process a POST request at the '/profile' route using postman update a user's details.
	Postman
*/

	url: http://localhost:4000/courses/search
	method: POST
	body: raw + JSON
		{
			"courseName": "HTML Course"
		}

/*
	Important Note:
		- Use postman to test the route then try to check mongodb
*/
/*
11. Add the following erratic codes in boodle notes and allow students to copy. Use chatGPT to debug the codes.
*/
	/*
		11a. Copy and paste the erratic route code from boodle notes.
			Application > routes > course.js
	*/
		/*...*/
		router.post('/:courseId/enrolled-users', getEmailsOfEnrolledUsers;

	/*
		11a. Copy and paste the generated controller code from boodle notes.
			Application > controllers > course.js
	*/
		/*...*/
		const getEmailsOfEnrolledUsers = async (req, res) => {
			const courseId = req.body.courseId;
		
			try {
			// Find the course by courseId
			const course = await Course.findById(courseId);
		
			if (!course) {
				return res.status(404).json({ message: 'Course not found' });
			}
		
			// Get the userIds of enrolled users from the course
			const userIds = course.enrollees.map(enrollee => enrollee.userId);
		
			// Find the users with matching userIds
			const enrolledUsers = await User.find({ _id: { $in: users } });
		
			// Extract the emails from the enrolled users
			const emails = enrolledStudents.forEach(user => user.email);
		
			res.status(200).json({ userEmails });
			} catch (error) {
			res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
			}
		}; 
	/*
		11c. Copy and paste the debugged route code from chatGPT. 
			Application > routes > course.js
	*/
		//Contextualize it to use our courseController.
		//Update to use get instead.
		router.get('/:courseId/enrolled-users', courseController.getEmailsOfEnrolledUsers);

	/*
		11d. Copy and paste the debugged controller code from chatGPT. 
			Application > controllers > course.js
	*/
		//Import the User model
		const User = require("../models/User");

		/*...*/

		//Contextualize it to use our module export approach.
		module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
			const courseId = req.params.courseId; // Use req.params instead of req.body
		
			try {
			// Find the course by courseId
			const course = await Course.findById(courseId);
		
			if (!course) {
				return res.status(404).json({ message: 'Course not found' });
			}
		
			// Get the userIds of enrolled users from the course
			const userIds = course.enrollees.map(enrollee => enrollee.userId);
		
			// Find the users with matching userIds
			const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Use userIds variable instead of undefined "users"
		
			// Extract the emails from the enrolled users
			const emails = enrolledUsers.map(user => user.email); // Use map instead of forEach
		
			res.status(200).json({ userEmails: emails }); // Use the correct variable name userEmails instead of emails
			} catch (error) {
			res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
			}
		};

/*
10. Login and copy a token to process a POST request at the '/profile' route using postman update a user's details.
	Postman
*/

url: http://localhost:4000/courses/<courseId>/enrolled-users
method: GET

/*
Important Note:
	- Use postman to test the route then try to check mongodb
*/

/*
========
Activity
========
*/

/*

Activity References

	- Tips for Improving Your Coding With ChatGPT
		https://betterprogramming.pub/10-tips-for-improving-your-coding-with-chatgpt-3e589de3aff3

Activity:

1. Using chatGPT, generate code to add the following features in our ExpressJS Booking API, provide chatGPT with the needed model from our app in your prompt:

2. Implement a way to let an admin user update another user as an admin.
	- The user id is passed in the request body.
	- The route needs an admin token.
	- Returns a message if update is successful.

3. Implement a search functionality for courses. Allow users to search for courses based on price range. 
	- Price range can be added in request body.
		- maxPrice and minPrice
	- All users can access this route.
	- Returns an array of courses with prices within the given price range.

4. Create a route and controller to update the enrollment status of a user for a specific course. 
	- This will allow an admin to mark a user as enrolled or completed or cancelled.
	- You can pass the status with the request body.
	- You can pass the id of the user in the request body.
	- You can pass the id of the course in the request body.

5. Provide a txt/readme file that contains the following:
	- Regular user email and password
	- Admin user email and password

6. Update your local sessions repo.
7. Push to git with the commit message of Add activity code s48.
8. Add the repo link in Boodle for s48.


*/
/*
	Solutions may vary from each student. Use postman to check each route.
*/

