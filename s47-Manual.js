/*
===========================================
S47 - Express.js - API Development (Part 5)
===========================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	JavaScript async And await Keywords
		https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a route for enrolling a user.
	Application > routes > user.js
*/

		/*...*/

		router.get("/details", auth.verify, (req, res) => {
			/*...*/
		});

		//[SECTION] Route to enroll user to a course
		router.post('/enroll', verify, userController.enroll);

		module.exports = router;

/*
2. Create a controller method for enrolling the user to a specific course.
	Application > controllers > user.js
*/

		const User = require("../models/User");
		const Course = require("../models/Course");
		const bcrypt = require("bcrypt");
		/*...*/

		module.exports.getProfile = (data) => {
			/*...*/
		};

		//[SECTION] Enroll user to a class
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Add the course ID to the user's enrollment array
			3. Update the document in the MongoDB Atlas Database
		*/



		// async - async keyword allows us to make our function asynchronous. which means, that instead of JS regular behaviour of running eac code line by line, It will allow us to wait for the result of function.
		// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user

		module.exports.enroll = async (req, res) => {
  		//get the details of the user who's trying to enroll in req.user
		/*
		This will be found in jwt authentication:
		    req.user 
		        {
		          	id,
		          	email,
		          	isAdmin
		        }
		*/

			console.log(req.user.id) //the user's id from the decoded token after verify()
			console.log(req.body.courseId) //the course from our request body

	  		//process stops here and sends response IF user is an admin
	  		if(req.user.isAdmin){
	    		return res.send("Action Forbidden")
	  		}

  

  

		//[SECTION] Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
		// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
		//await -  we use this to wait for the result of a function to finsih before proceeding to the next statement.

  

  			let isUserUpdated = await User.findById(req.user.id).then(user => {



		    	//[SECTION] Add the courseId in an object and push that object into the user's enrollment array:
			    let newEnrollment = {
			        courseId: req.body.courseId,
			        courseName: req.body.courseName,
			        courseDescription: req.body.courseDescription,
			        coursePrice: req.body.coursePrice
			    }

			    //[SECTION] access the enrollments array from our user and push the new enrollment object into the array
			    user.enrollments.push(newEnrollment);

			    //[SECTION] save the changes made to our user document and return the value of saving our document
			    //if we properly saved our document, isUserUpdated will contain the boolean true
			    //if we catch an error, isUserUpdated will contain the error message
			    return user.save().then(user => true).catch(err => err.message)


  			})

		//if isUserUpdated contains the boolean true, then the saving of our user document successful
		//if isUserUpdated does not contain the boolean true, we will stop our process and return a res.send() to our client with our message
			if(isUserUpdated !== true) {
			    return res.send({ message: isUserUpdated })
			}


			//Find the course we will push for our enrollee array 
			let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {


			    let enrollee = {
			        userId: req.user.id
			    }

	    		course.enrollees.push(enrollee);

	    		return course.save().then(course => true).catch(err => err.message)

  			})

		    //Stop the process if their was an error saving our course document
		    if(isCourseUpdated !== true) {
		        return res.send({ message: isCourseUpdated})
		    }

		  	if(isUserUpdated && isCourseUpdated) {
		      	return res.send({ message: "Enrolled Successfully."})
		  	}
		}


		/*
		Important Note
			- Refer to "references" section of this file to find the documentation for JavaScript async And await Keywords.
		*/

/*
3. Process a POST request at the "/enroll" route using postman to enroll a user to a course.
	Postman
*/

		url: http://localhost:4000/users/enroll
		method: POST
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
		body: raw + JSON
			{
				"courseId" : "613e926a82198824c8c4ce0e"
			}
		


/*
========
Activity
========
*/

/*

Activity References

Mongoose findbyId
https://mongoosejs.com/docs/api/model.html#Model.findById()

Activity:

Member 1:
1. Update your local sessions git repo, push with the commit message, "Add discussion s46"

Member 1,2,3,4,5
2. Create a GET method route "/getEnrollments" to retrieved logged user's enrolled courses.
- Note that only logged in user can use this route
- GET method routes should not pass a request body.
3. Add the function "getEnrollments" to get user's enrolled courses.
	Steps:
		a. Retrieve the course that matches the User ID provided in JWT
		b. Get all the enrolled courses of the User

Member 5:
4. Process a get request at the correct url to test the new route and controller.
- Save the requests as s47-requests

All members:
5. Check out to your own git branch with git checkout -b <branchName>.
6. Update your local sessions git repository and push to git with the commit message of Add activity code s45.
7. Add the sessions repo link in Boodle for s47.



*/

/*
Solution:

1. Create a route "/getEnrollments" to retrieved logged user's enrolled courses.
	Application > routes > course.js
*/

		/*...*/
			router.post("/enroll", verify, /*...*/);

		//[ACTIVITY] Get Logged User's Enrollments
			router.get('/getEnrollments', verify, userController.getEnrollments)




/*
2. Add the function "getEnrollments" to get user's enrolled courses.
	Application > controllers > course.js
*/

		//[ACTIVITY] Getting user's enrolled courses
		/*
			Steps:
			1. Retrieve the course that matches the User ID provided in JWT
			2. Get all the enrolled courses of the User
		*/
			module.exports.getEnrollments = (req, res) => {
			    User.findById(req.user.id)
			    .then(result => res.send(result.enrollments))
			    .catch(err => res.send(err))
			}


/*
3. Process a POST request at the "/getEnrollments" route using postman to enroll a user to a course.
	Postman
*/

		url: http://localhost:4000/users/enroll
		method: POST
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
		body: raw + JSON
			{
				"courseId" : "613e926a82198824c8c4ce0e"
			}

