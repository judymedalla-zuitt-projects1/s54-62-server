/*
===========================================
S44 - Express.js - API Development (Part 2)
===========================================
Booking System API - User Registration, Authentication, and JWT's
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/17dvAZ_Ru0RhQn4pPjeOL6cnP4AEJ4UdDLRGD3JB6neI/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		

Other References:
	Express JS Routing
		https://expressjs.com/en/guide/routing.html
	Mongoose Queries
		https://mongoosejs.com/docs/queries.html
	Mongoose find Method
		https://mongoosejs.com/docs/api.html#model_Model.find
	JavaScript then Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then
	Mongoose Models
		https://mongoosejs.com/docs/models.html
	bcrypt Package
		https://www.npmjs.com/package/bcrypt
	JSON Web Tokens
		https://jwt.io/
	jsonwebtoken Package
		https://www.npmjs.com/package/jsonwebtoken
	JavaScript typeof Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/


/*
1. Create a "routes" folder, create a "user.js" file to store the routes for our users and create a "checkEmail" route.
	Application > routes > user.js
*/

	//[SECTION] Dependencies and Modules
		const express = require("express");

	//[SECTION] Routing Component
		const router = express.Router();

	//[SECTION] Routes- POST
		// Route for checking if the user's email already exists in the database
		// Invokes the checkEmailExists function from the controller file to communicate with our database
		// Passes the "body" property of our "request" object to the corresponding controller function
		router.post("/checkEmail", (req, res) => {
			userController.checkEmailExists(req.body);
		})



	//[SECTION] Export Route System
		// Allows us to export the "router" object that will be accessed in our "index.js" file
		module.exports = router;

		/*
		Important Note
			- All files pertaining to our users are named "user" to follow good practice and standards for naming our files.
			- This is confusing for beginners but may be packaged to them by the way the files are accessed. (e.g. "/models/User" refers to the user model and "/routes/user" refers to the user routes file)
			- You may change the name of the files (e.g. "userRoutes" for the route file and userController for the controller file) for the student's convenience but make sure to highlight the importance of using these naming conventions which might confuse them when they encounter it in real world application.
			- Using these naming conventions also makes it easier during development which shortens the syntax.
			- Refer to "references" section of this file to find the documentation for Express JS Routing.
		*/

/*
2. Implement the main route for our user routes.
	Application > index.js
*/

		/*...*/
		const cors = require("cors");
		// Allows access to routes defined within our application
		const userRoutes = require("./routes/User");

		const app = express();

		/*...*/

		mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

	//[SECTION] Backend Routes 
		//http://localhost:4000/users
		// Defines the "/users" string to be included for all user routes defined in the "user" route file
		app.use("/users", userRoutes);



	//[SECTION] Server Gateway Response
		if(require.main === module) {
			app.listen( process.env.PORT || port, () => {
				console.log(`API is now online on port ${ process.env.PORT || port }`)
			});
		}

		module.exports = app;
/*
3. Create a "controllers" folder, create a "user.js" file to store the controller functions that will contain the business logic for our user CRUD operations.
	Application > controllers > user.js
*/

	//[SECTION] Dependencies and Modules
		// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
		const User = require("../models/User");

	//[SECTION] Check if the email already exists
		/*
			Steps: 
			1. Use mongoose "find" method to find duplicate emails
			2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
		*/
		module.exports.checkEmailExists = (reqBody) => {

			// The result is sent back to the frontend via the "then" method found in the route file
			return User.find({ email : reqBody.email }).then(result => {

				// The "find" method returns a record if a match is found
				if (result.length > 0) {

					return true;

				// No duplicate email found
				// The user is not yet registered in the database
				} else {

					return false;
				};
			});
		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for Mongoose Queries, Mongoose find Method and JavaScript then Method.
		*/

/*
4. Send the response back to our frontend application.
	Application > routes > user.js
*/

	//[SECTION] Dependencies and Modules
		const express = require('express');
		const userController = require("../controllers/user");

		/*...*/

	//[SECTION] Routes - POST

		// The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
		// The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
		router.post("/checkEmail", (req, res) => {
			userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
		});

		/*...*/

		/*
		Important Note:
			- Highlight the process of creating models, routes and controllers when developing a backend application to reinforce the process of routes and controller creation.
		*/

/*
5. Process a POST request at the "/checkEmail" route using postman to check for duplicate emails.
	Postman
*/

		url: http://localhost:4000/users/checkEmail
		method: POST
		body: raw + JSON
			{
			    "email": "john@mail.com"
			}

/*
6. Create a route for registering a user.
	Application > routes > user.js
*/

		/*...*/

		router.post("/checkEmail", (req, res) => {
			/*...*/
		});

		//[SECTION] Route for user registration
		router.post("/register", (req, res) => {
			userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
		});

		module.exports = router;

/*
7. Create a controller method for registering a user.
	Application > controllers > user.js
*/

		/*...*/

		module.exports.checkEmailExists = (reqBody) => {
			/*...*/
		};

		//[SECTION] User registration
		/*
			Steps:
			1. Create a new User object using the mongoose model and the information from the request body
			2. Make sure that the password is encrypted
			3. Save the new User to the database
		*/
		module.exports.registerUser = (reqBody) => {

			// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
			// Uses the information from the request body to provide all the necessary information
			let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				mobileNo : reqBody.mobileNo,
				password : reqBody.password
			})

			// Saves the created object to our database
			return newUser.save().then((user, error) => {

				// User registration failed
				if (error) {
					return false;

				// User registration successful
				} else {
					return true;
				}
			})
			.catch(err => err)
		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for Mongoose Models.
		*/

/*
8. Process a POST request at the "/register" route using postman to create a user record.
	Postman
*/

		url: http://localhost:4000/users/register
		method: POST
		body: raw + JSON
			{
				"firstName": "John",
				"lastName": "Smith",
			    "email": "john@mail.com",
			    "mobileNo": "09123456789",
			    "password": "john1234"
			}

/*
9. Install the "bcrypt" package.
	Application > Terminal
*/

		npm install bcrypt

		/*
		Important Note
			- In our application we will be using the bcrypt package to demonstrate how to encrypt password data when registering a user.
			- The "bcrypt" package is one of the many packages that we can use to encrypt information but is not commonly recommended because of how simple the algorithm is for creating encrypted passwords which have been decoded by hackers.
			- There are other more advanced encryption packages that can be used.
			- This is used in the bootcamp to easily demonstrate how encryption works.
			- Refer to "references" section of this file to find the documentation for bcrypt Package.
		*/

/*
10. Implement "bcrypt" in our application and refactor the "register" controller method.
	Application > controllers > user.js
*/

	//[SECTION] Dependencies and Modules
		/*...*/
		const bcrypt = require('bcrypt');

		module.exports.registerUser = (reqBody) => {

			let newUser = new User({
				/*...*/
				// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
				password : bcrypt.hashSync(reqBody.password, 10)
			})

			/*...*/

		};

		/*
		Important Note:
			- Make sure to delete all user records in the database before registering for a new user to ensure that all user records found in it would have encrypted passwords.
		*/

/*
11. Install the "jsonwebtoken" package.
	Application > Terminal
*/

		npm install jsonwebtoken

		/*
		Important Note
			- JSON web tokens are an industry standard for sending information between our applications in a secure manner.
			- The "jsonwebtoken" package will allow us to gain access to methods that will help us create a JSON web token.
			- Refer to "references" section of this file to find the documentations for JSON Web Tokens and jsonwebtoken Package.
		*/

/*
12. Create an "auth.js" file to store the methods for creating jsonwebtokens.
	Application > auth.js
*/

		const jwt = require("jsonwebtoken");
		// User defined string data that will be used to create our JSON web tokens
		// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
		const secret = "CourseBookingAPI";

		// [Section] JSON Web Tokens
		/*
		- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
		- Information is kept secure through the use of the secret code
		- Only the system that knows the secret code that can decode the encrypted information

		- Imagine JWT as a gift wrapping service that secures the gift with a lock
		- Only the person who knows the secret code can open the lock
		- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
		- This ensures that the data is secure from the sender to the receiver
		*/

		// Token creation
		/*
		- Analogy
			Pack the gift and provide a lock with the secret code as the key
		*/
		module.exports.createAccessToken = (user) => {
			// The data will be received from the registration form
			// When the user logs in, a token will be created with user's information
			const data = {
				id : user._id,
				email : user.email,
				isAdmin : user.isAdmin
			};

			// Generate a JSON web token using the jwt's sign method
			// Generates the token using the form data and the secret code with no additional options provided
			return jwt.sign(data, secret, {});
			
		};

/*
This is optional: 
============================================================================
	Notes on JWT:

	1. You can only get a unique jwt with our secret if you log in to our app with the correct email and password.

	2. As a user, You can only get your own details from your own token from logging in.

	3. JWT is not meant to store sensitive data. For now, for ease of use and for our MVP, we add the email and isAdmin details of the logged in user, however, in the future, you can limit this to only the id and for every route and feature, you can simply lookup for the user in the database to get his details.

	4. JWT is like a more secure passport you use around the app to access certain features meant for your type of user.

	5. We will verify the legitimacy of a JWT every time a user access a restricted feature. Each JWT contains a secret only our server knows. IF the jwt has been, in any way, changed, We will reject the user and his tampered token. IF the jwt, does not contain a secret OR the secret is different, we will reject his access and token.
============================================================================
*/


/*
13. Create a route for authenticating a user.
	Application > routes > user.js
*/

		/*...*/

		router.post("/register", (req, res) => {
			/*...*/
		});

		//[SECTION] Route for user authentication

			// Here we have streamlined the login routes by directly invoking the loginUser function. Consequently, the req.body request will now be incorporated into the controller function.
			router.post("/login", userController.loginUser);

			

		/*...*/

/*
14. Create a controller method for authenticating a user.
	Application > controllers > user.js
*/

		/*...*/
		const bcrypt = require("bcrypt");
		const auth = require("../auth");

		module.exports.checkEmailExists = (reqBody) => {
			/*...*/
		};

		module.exports.registerUser = (reqBody) => {
			/*...*/
		};

		//[SECTION] User authentication
		/*
			Steps:
			1. Check the database if the user email exists
			2. Compare the password provided in the login form with the password stored in the database
			3. Generate/return a JSON web token if the user is successfully logged in and return false if not
		*/
		module.exports.loginUser = (req, res) => {
			// The "findOne" method returns the first record in the collection that matches the search criteria
			// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
			return User.findOne({ email : req.body.email }).then(result => {

				// User does not exist
				if(result == null){

					return false;

				// User exists
				} else {

					// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
					// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
					// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
						//example. isSingle, isDone, isAdmin, areDone, etc..
					const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

					// If the passwords match/result of the above code is true
					if (isPasswordCorrect) {

						// Generate an access token
						// Uses the "createAccessToken" method defined in the "auth.js" file
						// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
						return res.send({ access : auth.createAccessToken(result) })

					// Passwords do not match
					} else {

						return res.send(false);

					}

				}

			})
			.catch(err => res.send(err))
		};

/*
15. Process a POST request at the "/login" route using postman to authenticate a user.
	Postman
*/

		url: http://localhost:4000/users/login
		method: POST
		body: raw + JSON
			{
			    "email": "john@mail.com",
			    "password": "john1234"
			}

/*
========
Activity
========
*/

/*

Activity Reference:

Express JS Routing
	https://expressjs.com/en/guide/routing.html
Mongoose Queries
	https://mongoosejs.com/docs/queries.html

Activity

Member 1:
1. Update your local sessions git repo, push with the commit message, "Add discussion s44"
2. Create a /details route that will accept the user’s Id to retrieve the details of a user.

Member 2,3,4:
3. Create a getProfile controller method for retrieving the details of the user:
	a. Find the document in the database using the user's ID
	b. Reassign the password of the returned document to an empty string
	c. Return the result back to the frontend
	
Member 5:
4. Process a POST request at the /details route using postman to retrieve the details of the user.
	- Save your requests in a collection with S44-Requests as title.

All Members:
5. Check out to your own git branch with git checkout -b <branchName>.
6. Update your local sessions git repository and push to git with the commit message of Add activity code s44.
7. Add the sessions repo link in Boodle for s44.


*/

/*
Solution:

1. Create a route for retrieving the details of a user.
	Application > routes > user.js
*/

		/*...*/

		router.post("/login", (req, res) => {
			/*...*/
		});

		//[ACTIVITY] Route for retrieving user details
			router.post("/details", userController.getProfile);

		/*...*/

/*
2. Create a controller method for retrieving the details of the user.
	Application > controllers > user.js
*/

		/*...*/

		module.exports.loginUser = (reqBody) => {
			/*...*/
		}

		// Retrieve user details
		/*
			Steps:
			1. Find the document in the database using the user's ID
			2. Reassign the password of the returned document to an empty string
			3. Return the result back to the frontend
		*/
		module.exports.getProfile = (req, res) => {


			return User.findById(req.body.id)
			.then(result => {

				// Changes the value of the user's password to an empty string when returned to the frontend
				// Not doing so will expose the user's password which will also not be needed in other parts of our application
				// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
				result.password = "";

				// Returns the user information with the password as an empty string
				return res.send(result);

			})
			.catch(err => res.send(err))
		};


/*
3. Process a POST request at the "/details" route using postman to retrieve the details of the user.
	Postman
*/

		url: http://localhost:4000/users/details
		method: POST
		body: raw + JSON
			{
			    "id": "613e77c9423177a5ab9a066d"
			}