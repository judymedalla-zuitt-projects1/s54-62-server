/*
===========================================
S46 - Express.js - API Development (Part 4)
===========================================
Booking System API - Courses CRUD
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1_CspcyqQ0SLYMuRBUgFMVgPAjXDs_9NF9X4sOscRYug/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	Mongoose Queries
		https://mongoosejs.com/docs/queries.html
	Mongoose find Method
		https://mongoosejs.com/docs/api.html#model_Model.find
	Mongoose findById Method
		https://mongoosejs.com/docs/api.html#model_Model.findById
	Mongoose findByIdAndUpdate Method
		https://mongoosejs.com/docs/api.html#model_Model.findByIdAndUpdate
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create a route for retrieving all the courses. This is for the Admin only
	Application > routes > course.js
*/

		/*...*/

		router.post("/", auth.verify, (req, res) => {
			/*...*/
		});

	//[SECTION] Route for retrieving all the courses
		router.get("/all", courseController.getAllCourses);

		module.exports = router;

/*
2. Create a controller method for retrieving all the courses for Admin only.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.addCourse = (data) => {
			/*...*/
		}

		// Retrieve all courses
		/*
			Steps:
			1. Retrieve all the courses from the database
		*/

		  //We will use the find() method of our Course model
		  //Because the Course model is connected to our courses collection
		  //Course.find({}) = db.courses.find({})
		  //empty {} will return all documents
		module.exports.getAllCourses = (req, res) => {
			return Course.find({}).then(result => {
				return res.send(result);
			});
		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentations for Mongoose Queries and Mongoose find Method.
		*/

/*
3. Process a GET request at the "/all" route using postman to retrieve all the courses.
	Postman
*/

		url: http://localhost:4000/courses/all
		method: GET
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOGIyOTA5YzZlNmE3MGFhYmUwNmFkMyIsImVtYWlsIjoiYWRtaW5AbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2ODQ5MTgxNzV9._mwPdLeWUYMIGwIbK8CDlAJPXnFI6RSnJrDL5Mz9lKw

/*
4. Create a route for retrieving all the active courses for the users.
	Application > routes > course.js
*/

		/*...*/

		// Route for retrieving all the courses
		router.get("/all", (req, res) => { //need for middleware
			/*...*/
		});

		// Route for retrieving all the ACTIVE courses for all users
		// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
		router.get("/", courseController.getAllActive);

		module.exports = router;

/*
5. Create a controller method for retrieving all the active courses.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.getAllCourses = () => {
			/*...*/
		};

		// Retrieve all ACTIVE courses
		/*
			Steps:
			1. Retrieve all the courses from the database with the property of "isActive" to true
		*/
		module.exports.getAllActive = () => {
			return Course.find({ isActive : true }).then(result => {
				return res.send(result);
			});
		};

/*
6. Process a GET request at the "/courses" route using postman to retrieve all the active courses.
	Postman
*/

		url: http://localhost:4000/courses
		method: GET

/*
7. Create a route for retrieving the details of a specific course.
	Application > routes > course.js
*/

		/*...*/

		router.get("/", (req, res) => {
			/*...*/
		});

		//[SECTION] Route for retrieving a specific course
		// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
		//[SECTION] Route for retrieving a specific course
		router.get("/:courseId", courseController.getCourse);
			module.exports = router;

/*
8. Create a controller method for retrieving a specific course.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.getAllActive = () => {
			/*...*/
		};

		//[SECTION] Retrieving a specific course
		/*
			Steps:
			1. Retrieve the course that matches the course ID provided from the URL
		*/
		module.exports.getCourse = (req, res) => {

			// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
				// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
					// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
					// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
			return Course.findById(req.params.courseId).then(result => {
				return res.send(result);
			});

		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentation for Mongoose findById Method.
		*/

/*
9. Process a GET request at the "/courseId" route using postman to retrieve all the active courses.
- Copy the id of course available to retrieve.
	Postman
*/

		url: http://localhost:4000/courses/613e926a82198824c8c4ce0e
		method: GET

/*
10. Create a route for updating a course for Admin only.
	Application > routes > course.js
*/

		/*...*/

		router.get("/:courseId", (req, res) => {
			/*...*/
		});

		//[SECTION] Route for updating a course (Admin)
		//add JWT authentication for verifying if it is an admin or not. Admin will only have an access to update a course.
		router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

		module.exports = router;

/*
11. Create a controller method for updating a course.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.getCourse = (reqParams) => {
			/*...*/
		};

		// Update a course
		/*
			Steps:
			1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
			2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
		*/
		// Information to update a course will be coming from both the URL parameters and the request body
		module.exports.updateCourse = (req, res) => {
			// Specify the fields/properties of the document to be updated
			let updatedCourse = {
				name : req.body.name,
				description	: req.body.description,
				price : req.body.price
			};

			// Syntax
				// findByIdAndUpdate(document ID, updatesToBeApplied)
			return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) => {

				// Course not updated
				if (error) {
					return res.send(false);

				// Course updated successfully
				} else {				
					return res.send(true);
				};
			});
		};

		/*
		Important Note
			- Refer to "references" section of this file to find the documentation for Mongoose findByIdAndUpdate Method.
		*/

/*
12. Process a PUT request at the "/courses" route using postman to create a new course.
	Postman
*/

		url: http://localhost:4000/courses/613e926a82198824c8c4ce0e
		method: POST
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ
		body: raw + JSON
			{
			    "name" : "JavaScript",
				"description" : "Making websites interactive since the 90's.",
				"price" : 2000
			}

/*
========
Activity
========
*/

/*

Activity Reference

Mongoose findById Method
	https://mongoosejs.com/docs/api.html#model_Model.findById
Mongoose findByIdAndUpdate Method
	https://mongoosejs.com/docs/api.html#model_Model.findByIdAndUpdate

Activity

Member 1:
1. Update your local sessions git repo, push with the commit message, "Add discussion s46"

For Archiving a Course

Member 1,2:
2. Create a route "/:courseId/archive" for archiving a course using PUT method. Establish a route that exclusively deactivates the course. This route should implement JWT authentication and extract the course ID from the URL. Note that only administrators are allowed to archive courses.
3. Create a controller method "archiveCourse" for archiving a course obtaining the course ID from the request params.
	- Soft Delete/Deactivating a course by simply updating the course "isActive" status into "false"

For Activating a Course

Member 3,4:
4. Create a route "/:courseId/activate" for activating a course using PUT method. Create a route that restores courses that have been deactivated. Note that only administrators are allowed to activate courses.
5. Create a controller "method activateCourse" for activating a course obtaining the course ID from the request params.
	- Simply update the course "isActive" status into "true"

Member 5:
6. Process a PUT request at the /:courseId/archive route using postman to archive a course
7. Process a PUT request at the /:courseId/activate route using postman to activate a course

All members:
8. Check out to your own git branch with git checkout -b <branchName>.
9. Update your local sessions git repository and push to git with the commit message of Add activity code s45.
10. Add the sessions repo link in Boodle for s46.

*/


/*
Solution:

For Archiving a Course

1. Create a route for archiving a course for Admin.
	Application > routes > course.js
*/

		/*...*/

		router.put("/:courseId", auth.verify, (req, res) => {
			/*...*/
		});

		// [ACTIVITY] Route to archiving a course (Admin)
		// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
		router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

		module.exports = router;

/*
2. Create a controller method for archiving a course.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.updateCourse = (reqParams, reqBody) => {
			/*...*/
		};

		//[SECTION] Archive a course
		// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
		// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
		// This allows us access to these records for future use and hides them away from users in our frontend application
		// There are instances where hard deleting records is required to maintain the records and clean our databases
		// The use of "hard delete" refers to removing records from our database permanently
		module.exports.archiveCourse = (req, res) => {

			let updateActiveField = {
				isActive: false
			}

			return Course.findByIdAndUpdate(req.params.courseId, updateActiveField)
			.then((course, error) => {

				//course archived successfully
				if(error){
					return res.send(false)

				// failed
				} else {
					return res.send(true)
				}
			})
			.catch(err => res.send(err))

		};

/*
3. Process a PUT request at the "/courseId/archive" route using postman to archive a course.
	Postman
*/

		url: http://localhost:4000/courses/613e926a82198824c8c4ce0e/archive
		method: PUT
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ


/*
For Activating a Course

1. Create a route for activating a course for Admin.
	Application > routes > course.js
*/

	//[ACTIVITY] Route to activating a course (Admin)
		router.put("/:courseId/activate", verify, verifyAdmin, courseController.archiveCourse);


/*
2. Create a controller method for activating a course.
	Application > controllers > course.js
*/

		/*...*/

		module.exports.updateCourse = (reqParams, reqBody) => {
			/*...*/
		};

		//[SECTION] Activate a course
		module.exports.activateCourse = (req, res) => {

			let updateActiveField = {
				isActive: true
			}

			return Course.findByIdAndUpdate(req.params.courseId, updateActiveField)
			.then((course, error) => {

				//course archived successfully
				if(error){
					return res.send(false)

				// failed
				} else {
					return res.send(true)
				}
			})
			.catch(err => res.send(err))

		};

/*
3. Process a PUT request at the "/courseId/archive" route using postman to archive a course.
	Postman
*/

		url: http://localhost:4000/courses/613e926a82198824c8c4ce0e/activate
		method: PUT
		headers:
			Type: Bearer Token
			Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U3N2M5NDIzMTc3YTVhYjlhMDY2ZCIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2MzE0ODkyNzV9._8QPUI5S0flEO3pMncEo2bQ_GzTQfzLUM_Z2s8ljAQQ


















